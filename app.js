const keyPublishable = 'pk_test_zyAEyqN3PWtVx00kxlg808P300tO8ps53l';
const keySecret = 'sk_test_ZoUG7BulqyTcBwv0XGXaDUHv00PWyDVNoi';
const app = require('express')();
const stripe = require("stripe")(keySecret);
stripe.setMaxNetworkRetries(2);
/**
 * Error Handling
 * --------------
 * Server Error  :  500
 * Network Error : Maximium retry set
 * Content Error  :4xx , 
 */
const pug = require('pug');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const path = require('path');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug')


app.get("/", ((req, res) => {
    // res.json({"index":"hi deat"}); 
    res.render('index');
}));
app.post("/charge", function (req, res) {
    let amount = 5 * 100;
    // create a customer 
    stripe.customers.create({
        email: req.body.stripeEmail, // customer email, which user need to enter while making payment
        source: req.body.stripeToken + 'sss' // token for the given card 
    }).then(customer =>
        stripe.charges.create({ // charge the customer
            "shd": "s",
            description: "Sample Charge",
            currency: "usd",
            customer: customer.id
        })
    ).then(charge => {
        console.log('charge = ', charge);
        res.render("charge")
    }).catch((err) => {
        throw new Error("Whoops!");
    });
});
app.listen(3000, () => {
    console.log(`App is running at: http://localhost:3000/`);
});